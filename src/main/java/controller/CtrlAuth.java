/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import at.favre.lib.crypto.bcrypt.BCrypt;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import lanceur.Main;
import modele.metier.Utilisateur;
import vue.auth_JFrame;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import static lanceur.Main.user;
import static lanceur.Main.emf;

/**
 * @author Jules
 */
public class CtrlAuth implements WindowListener, ActionListener {

    private final auth_JFrame vue;

    public CtrlAuth(auth_JFrame vue) {
        this.vue = vue;
        // le contrôleur écoute la vue
        this.vue.addWindowListener(this);
        this.vue.getjButtonValider().addActionListener(this);
    }

    public boolean seConnecter() {
        String login = vue.getjTextFieldLogin().getText();
        char[] password = vue.getjPasswordField().getPassword();

        EntityManager em = Main.em;

        TypedQuery<Utilisateur> query = em.createNamedQuery("Utilisateur.findByPseudoU", Utilisateur.class).setParameter("pseudoU", login);

        try {
            user = query.getSingleResult();
            
            BCrypt.Result result = BCrypt.verifyer().verify(password, removeLastDot(user.getMdpU()));
            if (!result.verified) {
                JOptionPane.showMessageDialog(null, "Mot de passe incorrect", "ERREUR", JOptionPane.ERROR_MESSAGE);
            } else {
                if (user.getFonction().getModerateur() || user.getFonction().getResponsable()) {
                    return true;
                }

            }

        } catch (Exception e) {
            if ("getSingleResult() did not retrieve any entities.".equals(e.getMessage())) {
                JOptionPane.showMessageDialog(null, "Login incorrect", "ERREUR", JOptionPane.ERROR_MESSAGE);
            }
        }

        return false;
    }

    public static String removeLastDot(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == '.' && str.length() == 61) {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    // REACTIONS EVENEMENTIELLES
    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        // Confirmer avant de quitter
        int rep = JOptionPane.showConfirmDialog(null, "Quitter l'application\nEtes-vous sûr(e) ?", "Modération Resto.fr", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (rep == JOptionPane.YES_OPTION) {
            // mettre fin à l'application
            emf.close();
            System.exit(0);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(vue.getjButtonValider())) {

            if (seConnecter()) {
                vue.getjPasswordField().setText("");
                vue.getjTextFieldLogin().setText("");
                vue.setVisible(false);
                Main.afficherMainJFrame();
            }
        }

    }
}
