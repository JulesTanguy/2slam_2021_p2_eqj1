/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import jakarta.persistence.TypedQuery;
import lanceur.Main;
import modele.metier.Critiquer;
import vue.MainJFrame;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static lanceur.Main.em;
import static lanceur.Main.emf;

/**
 * @author Jules
 */
public class CtrlMainJFrame implements WindowListener, ActionListener, PropertyChangeListener, TableModelListener {

    public final MainJFrame vue;
    private Date dateBegin;
    private Date dateEnd;
    public static List<Critiquer> lesCritiques;

    public CtrlMainJFrame(MainJFrame vue) {
        this.vue = vue;
        // le contrôleur écoute la vue
        this.vue.addWindowListener(this);
        this.vue.getjButtonSupprimer().addActionListener(this);
        this.vue.getjDateChooserDateDonnee().addPropertyChangeListener(this);
        this.vue.getjDateChooserBegin().addPropertyChangeListener(this);
        this.vue.getjDateChooserEnd().addPropertyChangeListener(this);
        this.vue.getjButtonResetFilter().addActionListener(this);
        this.vue.getjButtonDeconnecter().addActionListener(this);
        this.vue.getJTableCritiques().getModel().addTableModelListener(this);
        this.vue.getjButtonSupprimerMasque().addActionListener(this);

        TypedQuery<Critiquer> query = em.createNamedQuery("Critiquer.findAllByDate", Critiquer.class);
        lesCritiques = query.getResultList();
    }

    public void afficherLesCritiques() {
        vue.getModeleTableCritiques().setRowCount(0);
        String[] titresColonnes = {"Utilisateur", "Commentaire", "Resto", "Date", "Masqué"};
        vue.getModeleTableCritiques().setColumnIdentifiers(titresColonnes);
        for (Critiquer critique : lesCritiques) {
            var ligneDonnees = new Object[5];
            ligneDonnees[0] = critique.getUtilisateur().getPseudoU();
            ligneDonnees[1] = critique.getCommentaire();
            ligneDonnees[2] = critique.getResto().getNomR();
            if (critique.getDate() != null) {
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                ligneDonnees[3] = dateFormat.format(critique.getDate());
            }
            if (critique.getMasquer() != null) {
                ligneDonnees[4] = critique.getMasquer();
            }
            vue.getModeleTableCritiques().addRow(ligneDonnees);
            vue.resizeColumnWidth(vue.getJTableCritiques());
        }
    }

    // REACTIONS EVENEMENTIELLES
    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        // Confirmer avant de quitter
        int rep = JOptionPane.showConfirmDialog(null, "Quitter l'application\nEtes-vous sûr(e) ?", "Modération Resto.fr", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (rep == JOptionPane.YES_OPTION) {
            // mettre fin à l'application
            emf.close();
            System.exit(0);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(vue.getjButtonSupprimer())) {
            if (vue.getJTableCritiques().getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(vue,
                        "Aucune critique sélectionée",
                        "Erreur",
                        JOptionPane.ERROR_MESSAGE);
            } else {
                em.getTransaction().begin();
                Critiquer laCritique = lesCritiques.get(vue.getJTableCritiques().getSelectedRow());
                lesCritiques.remove(vue.getJTableCritiques().getSelectedRow());
                em.remove(laCritique);
                em.getTransaction().commit();

                ((DefaultTableModel) vue.getJTableCritiques().getModel()).removeRow(vue.getJTableCritiques().getSelectedRow());
            }
        }

        if (e.getSource().equals(vue.getjButtonResetFilter())) {
            TypedQuery<Critiquer> query = em.createNamedQuery("Critiquer.findAllByDate", Critiquer.class);
            lesCritiques = query.getResultList();
            afficherLesCritiques();
        }

        if (e.getSource().equals(vue.getjButtonDeconnecter())) {
            vue.setVisible(false);
            Main.vueAuth.getjTextFieldLogin().requestFocus();
            Main.vueAuth.setVisible(true);
        }

        if (e.getSource().equals(vue.getjButtonSupprimerMasque())) {
            var query = em.createNamedQuery("Critiquer.countCritiqueMasques", Critiquer.class);
            int rep = JOptionPane.showConfirmDialog(null, "Cette action va supprimer " + query.getSingleResult() + " critiques\nEtes-vous sûr(e) ?", "Modération Resto.fr", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (rep == JOptionPane.YES_OPTION) {
                em.getTransaction().begin();
                em.createNamedQuery("Critiquer.DeleteWhereMasque", Critiquer.class).executeUpdate();
                em.getTransaction().commit();
                query = em.createNamedQuery("Critiquer.findAllByDate", Critiquer.class);
                lesCritiques = query.getResultList();
                afficherLesCritiques();
            }

        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent e) {
        if (e.getSource().equals(vue.getjDateChooserDateDonnee()) && e.getPropertyName().equals("date")) {
            TypedQuery<Critiquer> query = em.createNamedQuery("Critiquer.findByDate", Critiquer.class);
            query.setParameter("date", e.getNewValue());
            lesCritiques = query.getResultList();
            afficherLesCritiques();
        }

        if (e.getSource().equals(vue.getjDateChooserBegin()) && e.getPropertyName().equals("date")) {
            dateBegin = (Date) e.getNewValue();
        }

        if (e.getSource().equals(vue.getjDateChooserEnd()) && e.getPropertyName().equals("date")) {
            dateEnd = (Date) e.getNewValue();
        }

        if (dateBegin != null && dateEnd != null) {
            TypedQuery<Critiquer> query = em.createNamedQuery("Critiquer.findAllBetweenTwoDate", Critiquer.class);
            query.setParameter("startDate", dateBegin);
            query.setParameter("endDate", dateEnd);
            lesCritiques = query.getResultList();
            afficherLesCritiques();
            dateBegin = null;
            dateEnd = null;
        }

    }

    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() == TableModelEvent.UPDATE && vue.getJTableCritiques().getSelectedRow() >= 0) {
            em.getTransaction().begin();
            Critiquer laCritique = lesCritiques.get(vue.getJTableCritiques().getSelectedRow());
            if (laCritique.getMasquer() != null) {
                laCritique.setMasquer(!laCritique.getMasquer());
            } else {
                laCritique.setMasquer(true);
            }
            em.getTransaction().commit();
        }
    }
}
