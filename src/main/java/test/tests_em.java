package test;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;
import modele.metier.Critiquer;

import java.util.List;

/**
 * @author btssio
 */
public class tests_em {

    public static void main(String[] args) {

        EntityManager em;

        System.out.println("\nDEBUT DES TESTS DE PERSISTANCE POUR LA CLASSE ADRESSE");
        em = Persistence.createEntityManagerFactory("MainPU").createEntityManager();
        EntityTransaction tx = em.getTransaction();

        System.out.println("\nDEBUT DE LA TRANSACTION N°1");
        tx.begin();

        TypedQuery<Critiquer> query = em.createNamedQuery("Critiquer.findAll", Critiquer.class);
        List<Critiquer> results = query.getResultList();

        for (Critiquer critique : results) {
            System.out.println(critique.getUtilisateur().getPseudoU());
        }

    }

}
