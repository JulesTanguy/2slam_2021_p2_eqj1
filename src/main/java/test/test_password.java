/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

import at.favre.lib.crypto.bcrypt.BCrypt;

/**
 * @author Jules
 */
public class test_password {

    public static String removeLastDot(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == '.' && str.length() == 61) {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    public static void main(String[] args) {
        String password = "moderateur";
        String bcryptHashString = BCrypt.withDefaults().hashToString(10, password.toCharArray());
        System.out.println(bcryptHashString);
        String bcryptHashString2 = "$2y$10$KDjQ84SyQp2yKp.QILnetu9fFuad9oP19N6.8lKMvixo1GQaTX8Aq.";
        System.out.println(bcryptHashString2);
        BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), removeLastDot(bcryptHashString));
        System.out.println(result.verified);

    }

}
