/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lanceur;

import com.formdev.flatlaf.FlatLightLaf;
import controller.CtrlAuth;
import controller.CtrlMainJFrame;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import modele.metier.Utilisateur;
import vue.MainJFrame;
import vue.auth_JFrame;


/**
 * @author Jules
 */
public class Main {

    public static EntityManager em;
    public static EntityManagerFactory emf;
    public static MainJFrame vueMainJFrame;
    public static auth_JFrame vueAuth;
    public static Utilisateur user;

    public static void main(String[] args) {
        FlatLightLaf.setup();

        vueAuth = new auth_JFrame();
        new CtrlAuth(vueAuth);
        vueAuth.setVisible(true);
        
        emf = Persistence.createEntityManagerFactory("MainPU");
        em = emf.createEntityManager();
    }

    public static void afficherMainJFrame(){
        vueMainJFrame = new MainJFrame();
        CtrlMainJFrame CtrlMainJFrame = new CtrlMainJFrame(vueMainJFrame);

        CtrlMainJFrame.afficherLesCritiques();

        vueMainJFrame.setVisible(true);

    }
}
