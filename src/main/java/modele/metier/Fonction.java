/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modele.metier;

import jakarta.persistence.*;

import java.io.Serializable;

/**
 * @author Jules
 */
@Entity
@Table(name = "fonction")
@NamedQueries({
        @NamedQuery(name = "Fonction.findAll", query = "SELECT f FROM Fonction f"),
        @NamedQuery(name = "Fonction.findByIdU", query = "SELECT f FROM Fonction f WHERE f.idU = :idU"),
        @NamedQuery(name = "Fonction.findByModerateur", query = "SELECT f FROM Fonction f WHERE f.moderateur = :moderateur"),
        @NamedQuery(name = "Fonction.findByResponsable", query = "SELECT f FROM Fonction f WHERE f.responsable = :responsable")})
public class Fonction implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idU")
    private Long idU;
    @Column(name = "moderateur")
    private Boolean moderateur;
    @Column(name = "responsable")
    private Boolean responsable;
    @JoinColumn(name = "idU", referencedColumnName = "idU", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Utilisateur utilisateur;

    public Fonction() {
    }

    public Fonction(Long idU) {
        this.idU = idU;
    }

    public Long getIdU() {
        return idU;
    }

    public void setIdU(Long idU) {
        this.idU = idU;
    }

    public Boolean getModerateur() {
        return moderateur;
    }

    public void setModerateur(Boolean moderateur) {
        this.moderateur = moderateur;
    }

    public Boolean getResponsable() {
        return responsable;
    }

    public void setResponsable(Boolean responsable) {
        this.responsable = responsable;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idU != null ? idU.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fonction)) {
            return false;
        }
        Fonction other = (Fonction) object;
        if ((this.idU == null && other.idU != null) || (this.idU != null && !this.idU.equals(other.idU))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modele.metier.Fonction[ idU=" + idU + " ]";
    }

}
